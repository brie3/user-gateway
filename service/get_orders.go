package service

import (
	"log"
	"user-gateway/models"
	pbPayment "user-gateway/proto/payment"
	pbUser "user-gateway/proto/user"
	"user-gateway/restapi/operations/profile"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetOrders handles getmovie logic.
func (g *Gateway) GetOrders(params profile.GetOrdersParams, principal interface{}) middleware.Responder {
	u, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.RemoteAddr)
		return profile.NewGetOrdersUnauthorized()
	}
	if u.Role == int32(pbUser.UserRole_UserRole_NOTSET) {
		log.Printf(userValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email)
		return profile.NewGetOrdersForbidden()
	}
	po, err := g.PaymentClient.GetOrders(params.HTTPRequest.Context(), &pbPayment.OrdersRequest{Limit: params.Limit, Offset: swag.Int32Value(params.Offset), UserId: u.ID})
	if err != nil {
		log.Printf(ordersFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return profile.NewGetOrdersDefault(internalCode).WithPayload(wrapGRPCError(err))
	}

	mo := models.Orders{}
	if err := swag.DynamicJSONToStruct(&po, &mo); err != nil {
		log.Printf(jsonConvertOnGetOrdersFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return profile.NewGetOrdersDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return profile.NewGetOrdersOK().WithPayload(&mo)
}
