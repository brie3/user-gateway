package service

import (
	"log"
	"user-gateway/models"
	pbMovie "user-gateway/proto/movie"
	"user-gateway/restapi/operations/movies"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetMovies handles get movies logic.
func (g *Gateway) GetMovies(params movies.GetMoviesParams) middleware.Responder {
	mc, err := g.MovieClient.GetMovies(params.HTTPRequest.Context(), &pbMovie.MoviesRequest{
		Limit:           params.Limit,
		Offset:          swag.Int32Value(params.Offset),
		AgeRating:       pbMovie.AgeRating(swag.Int32Value(params.AgeRating)),
		Category:        pbMovie.Category(swag.Int32Value(params.Category)),
		FromReleaseYear: swag.Int32Value(params.FromReleaseYear),
		ToReleaseYear:   swag.Int32Value(params.ToReleaseYear),
		OnlyPaid:        swag.BoolValue(params.OnlyRented),
	})
	if err != nil {
		log.Printf(moviesFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, err)
		return movies.NewGetMoviesDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	mm := models.Movies{}
	if err := swag.DynamicJSONToStruct(&mc, &mm); err != nil {
		log.Printf(jsonConvertOnGetMoviesFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, err)
		return movies.NewGetMoviesDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewGetMoviesOK().WithPayload(&mm)
}
