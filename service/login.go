package service

import (
	"log"
	"user-gateway/models"
	pbAuth "user-gateway/proto/auth"
	"user-gateway/restapi/operations/auth"

	"github.com/go-openapi/runtime/middleware"
)

// Login handles login logic.
func (g *Gateway) Login(params auth.LoginUserParams) middleware.Responder {
	lr, err := g.AuthClient.Login(params.HTTPRequest.Context(), &pbAuth.LoginRequest{Email: params.Body.Email.String(), Pwd: params.Body.Password})
	if err != nil {
		log.Printf(loginFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, params.Body.Email, err)
		return auth.NewLoginUserDefault(badRequestCode).WithPayload(wrapGRPCError(err))
	}
	return auth.NewLoginUserOK().WithPayload(&models.LoginResponse{Jwt: lr.Jwt})
}
