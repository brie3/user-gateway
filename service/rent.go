package service

import (
	"log"
	"user-gateway/models"
	pbMovie "user-gateway/proto/movie"
	pbPayment "user-gateway/proto/payment"
	pbUser "user-gateway/proto/user"
	"user-gateway/restapi/operations/movies"

	"github.com/go-openapi/runtime/middleware"
)

// RentMovie handles rent movie logic.
func (g *Gateway) RentMovie(params movies.RentMovieParams, principal interface{}) middleware.Responder {
	u, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.RemoteAddr)
		return movies.NewRentMovieUnauthorized()
	}
	if u.Role == int32(pbUser.UserRole_UserRole_NOTSET) {
		log.Printf(userValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email)
		return movies.NewRentMovieForbidden()
	}
	uo, err := g.PaymentClient.CreateOrder(params.HTTPRequest.Context(), &pbPayment.CreateOrderRequest{UserId: u.ID, MovieId: params.MovieID, Amount: 200}) // TODO movie amount cost?
	if err != nil {
		log.Printf(rentFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return movies.NewRentMovieBadRequest()
	}

	rr, err := g.MovieClient.RentMovie(params.HTTPRequest.Context(), &pbMovie.RentMovieRequest{MovieId: uo.GetMovieId(), UserId: uo.GetUserId()})
	if err != nil {
		log.Printf(rentFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return movies.NewRentMovieDefault(internalCode).WithPayload(wrapGRPCError(err))
	}

	return movies.NewRentMovieOK().WithPayload(&models.RentMovieResponse{EndRentTime: rr.GetEndRentTime(), MovieID: rr.GetMovieId()})
}
