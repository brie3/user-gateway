package service

import (
	"log"
	"user-gateway/models"
	pbPayment "user-gateway/proto/payment"
	pbUser "user-gateway/proto/user"
	"user-gateway/restapi/operations/profile"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
)

// GetProfile handles profile getting logic.
func (g *Gateway) GetProfile(params profile.GetProfileParams, principal interface{}) middleware.Responder {
	u, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.RemoteAddr)
		return profile.NewGetProfileUnauthorized()
	}
	if u.Role == int32(pbUser.UserRole_UserRole_NOTSET) {
		log.Printf(userValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email)
		return profile.NewGetProfileForbidden()
	}
	pr, err := g.UserClient.GetUser(params.HTTPRequest.Context(), &pbUser.UserRequest{Id: u.ID, Email: u.Email})
	if err != nil {
		log.Printf(profileFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return profile.NewGetProfileDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	ub, err := g.PaymentClient.GetUserBalance(params.HTTPRequest.Context(), &pbPayment.GetUserBalanceRequest{UserId: u.ID})
	if err != nil {
		log.Printf(profileFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return profile.NewGetProfileDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return profile.NewGetProfileOK().WithPayload(&models.Profile{
		Balance:     float64(ub.GetBalance()),
		BirthDate:   pr.GetBirthDate(),
		DisplayName: pr.GetDispayName(),
		Email:       strfmt.Email(pr.GetEmail()),
		ID:          pr.GetId(),
		PhoneNumber: pr.GetPhoneNumber(),
		Role:        int32(pr.GetRole()),
		Status:      int32(pr.GetStatus()),
	})
}
