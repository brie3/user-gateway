package service

import (
	"log"
	"os"
	"user-gateway/models"
	"user-gateway/proto/auth"
	"user-gateway/proto/movie"
	"user-gateway/proto/payment"
	"user-gateway/proto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	badRequestCode                = 400
	unauthorizedCode              = 401
	internalCode                  = 500
	internalFormat                = "internal error"
	unauthorizeFormat             = "authorizing addr: %s"
	movieFormat                   = "[%s] get movie for addr: %s got error: %v"
	userValidationFormat          = "[%s] user role validation for addr: %q and email: %q"
	moviesFormat                  = "[%s] get movies for addr: %s got error: %v"
	rentFormat                    = "[%s] rent movie for addr: %s; email: %s got error: %v"
	loginFormat                   = "[%s] login for addr: %s; email: %s got error: %v"
	registerFormat                = "[%s] register for addr: %s; email: %s got error: %v"
	profileFormat                 = "[%s] get profile for addr: %s; email: %s got error: %v"
	ordersFormat                  = "[%s] get orders for addr: %s; email: %s got error: %v"
	paymentFormat                 = "[%s] get payments for addr: %s; email: %s got error: %v"
	jsonConvertOnGetMovieFormat   = "[%s] json convert on get movie response for addr: %q got error: %v"
	jsonConvertOnGetMoviesFormat  = "[%s] json convert on get movies response for addr: %q got error: %v"
	jsonConvertOnGetOrdersFormat  = "[%s] json convert on get orders response for addr: %q and email: %q got error: %v"
	jsonConvertOnGetPaymentFormat = "[%s] json convert on get payment response for addr: %q and email: %q got error: %v"
)

// RequestIDContextKey handles uuid for requests.
var RequestIDContextKey = struct{}{}

// Gateway handles proto interactions.
type Gateway struct {
	UserClient                                 user.UserServiceClient
	MovieClient                                movie.MovieServiceClient
	AuthClient                                 auth.AuthServiceClient
	PaymentClient                              payment.PaymentServiceClient
	userConn, paymentConn, movieConn, authConn *grpc.ClientConn
}

func New() *Gateway {
	var (
		addr string
		ok   bool
		err  error
	)
	gate := Gateway{}
	addr, ok = os.LookupEnv("USER_SERVICE_ADDR")
	if !ok {
		log.Fatalf("USER_SERVICE_ADDR env is not set")
	}
	gate.userConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to user service: %v", err)
	}

	addr, ok = os.LookupEnv("PAYMENT_SERVICE_ADDR")
	if !ok {
		log.Fatalf("PAYMENT_SERVICE_ADDR env is not set")
	}
	gate.paymentConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to payment service: %v", err)
	}

	addr, ok = os.LookupEnv("MOVIE_SERVICE_ADDR")
	if !ok {
		log.Fatalf("MOVIE_SERVICE_ADDR env is not set")
	}
	gate.movieConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to movie service: %v", err)
	}

	addr, ok = os.LookupEnv("AUTH_SERVICE_ADDR")
	if !ok {
		log.Fatalf("AUTH_SERVICE_ADDR env is not set")
	}
	gate.authConn, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("can't connect to auth service: %v", err)
	}

	gate.MovieClient = movie.NewMovieServiceClient(gate.movieConn)
	gate.AuthClient = auth.NewAuthServiceClient(gate.authConn)
	gate.UserClient = user.NewUserServiceClient(gate.userConn)
	gate.PaymentClient = payment.NewPaymentServiceClient(gate.paymentConn)
	return &gate
}

func (g *Gateway) Close() {
	log.Println(g.authConn.Close(), g.movieConn.Close(), g.userConn.Close(), g.paymentConn.Close())
}

func wrapGRPCError(err error) *models.Error {
	switch status.Code(err) {
	case codes.PermissionDenied:
		return &models.Error{Code: unauthorizedCode, Message: status.Convert(err).Message()}
	case codes.AlreadyExists, codes.InvalidArgument:
		return &models.Error{Code: badRequestCode, Message: status.Convert(err).Message()}
	default:
		return &models.Error{Code: internalCode, Message: internalFormat}
	}
}
