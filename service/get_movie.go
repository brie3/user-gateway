package service

import (
	"log"
	"user-gateway/models"
	pbMovie "user-gateway/proto/movie"
	"user-gateway/restapi/operations/movies"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetMovie handles get movie logic.
func (g *Gateway) GetMovie(params movies.GetMovieParams) middleware.Responder {
	gm, err := g.MovieClient.GetMovie(params.HTTPRequest.Context(), &pbMovie.MovieRequest{Id: params.MovieID})
	if err != nil {
		log.Printf(movieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, err)
		return movies.NewGetMovieDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	mm := models.Movie{}
	if err = swag.DynamicJSONToStruct(&gm, &mm); err != nil {
		log.Printf(jsonConvertOnGetMovieFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, err)
		return movies.NewGetMovieDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return movies.NewGetMovieOK().WithPayload(&mm)
}
