package service

import (
	"log"
	"user-gateway/models"
	pbPayment "user-gateway/proto/payment"
	pbUser "user-gateway/proto/user"
	"user-gateway/restapi/operations/profile"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

// GetPayments handles getpayments logic.
func (g *Gateway) GetPayments(params profile.GetPaymentsParams, principal interface{}) middleware.Responder {
	u, ok := principal.(*User)
	if !ok {
		log.Printf(unauthorizeFormat, params.HTTPRequest.RemoteAddr)
		return profile.NewGetPaymentsUnauthorized()
	}
	if u.Role == int32(pbUser.UserRole_UserRole_NOTSET) {
		log.Printf(userValidationFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email)
		return profile.NewGetPaymentsForbidden()
	}
	pp, err := g.PaymentClient.GetPayments(params.HTTPRequest.Context(), &pbPayment.PaymentsRequest{Limit: params.Limit, Offset: swag.Int32Value(params.Offset), UserId: u.ID})
	if err != nil {
		log.Printf(paymentFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return profile.NewGetPaymentsDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	mp := models.Payments{}
	if err := swag.DynamicJSONToStruct(&pp, &mp); err != nil {
		log.Printf(jsonConvertOnGetPaymentFormat, params.HTTPRequest.Context().Value(RequestIDContextKey).(string), params.HTTPRequest.RemoteAddr, u.Email, err)
		return profile.NewGetPaymentsDefault(internalCode).WithPayload(wrapGRPCError(err))
	}
	return profile.NewGetPaymentsOK().WithPayload(&mp)
}
