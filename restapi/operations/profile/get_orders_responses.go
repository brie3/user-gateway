// Code generated by go-swagger; DO NOT EDIT.

package profile

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"user-gateway/models"
)

// GetOrdersOKCode is the HTTP code returned for type GetOrdersOK
const GetOrdersOKCode int = 200

/*GetOrdersOK multiple orders

swagger:response getOrdersOK
*/
type GetOrdersOK struct {

	/*
	  In: Body
	*/
	Payload *models.Orders `json:"body,omitempty"`
}

// NewGetOrdersOK creates GetOrdersOK with default headers values
func NewGetOrdersOK() *GetOrdersOK {

	return &GetOrdersOK{}
}

// WithPayload adds the payload to the get orders o k response
func (o *GetOrdersOK) WithPayload(payload *models.Orders) *GetOrdersOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get orders o k response
func (o *GetOrdersOK) SetPayload(payload *models.Orders) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetOrdersOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// GetOrdersUnauthorizedCode is the HTTP code returned for type GetOrdersUnauthorized
const GetOrdersUnauthorizedCode int = 401

/*GetOrdersUnauthorized unauthorized access for a lack of authentication

swagger:response getOrdersUnauthorized
*/
type GetOrdersUnauthorized struct {
}

// NewGetOrdersUnauthorized creates GetOrdersUnauthorized with default headers values
func NewGetOrdersUnauthorized() *GetOrdersUnauthorized {

	return &GetOrdersUnauthorized{}
}

// WriteResponse to the client
func (o *GetOrdersUnauthorized) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(401)
}

// GetOrdersForbiddenCode is the HTTP code returned for type GetOrdersForbidden
const GetOrdersForbiddenCode int = 403

/*GetOrdersForbidden forbidden access for a lack of sufficient privileges

swagger:response getOrdersForbidden
*/
type GetOrdersForbidden struct {
}

// NewGetOrdersForbidden creates GetOrdersForbidden with default headers values
func NewGetOrdersForbidden() *GetOrdersForbidden {

	return &GetOrdersForbidden{}
}

// WriteResponse to the client
func (o *GetOrdersForbidden) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(403)
}

/*GetOrdersDefault other error response

swagger:response getOrdersDefault
*/
type GetOrdersDefault struct {
	_statusCode int

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetOrdersDefault creates GetOrdersDefault with default headers values
func NewGetOrdersDefault(code int) *GetOrdersDefault {
	if code <= 0 {
		code = 500
	}

	return &GetOrdersDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the get orders default response
func (o *GetOrdersDefault) WithStatusCode(code int) *GetOrdersDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the get orders default response
func (o *GetOrdersDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithPayload adds the payload to the get orders default response
func (o *GetOrdersDefault) WithPayload(payload *models.Error) *GetOrdersDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get orders default response
func (o *GetOrdersDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetOrdersDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
