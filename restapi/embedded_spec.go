// Code generated by go-swagger; DO NOT EDIT.

package restapi

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"
)

var (
	// SwaggerJSON embedded version of the swagger document used at generation time
	SwaggerJSON json.RawMessage
	// FlatSwaggerJSON embedded flattened version of the swagger document used at generation time
	FlatSwaggerJSON json.RawMessage
)

func init() {
	SwaggerJSON = json.RawMessage([]byte(`{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "This sample API implement basic user gateway for movie rent service.\n\nPersonas:\n  - user that want to see all movies that are free to match.\n  - registered user that want to consult personal account info and rent movies.\n\nThe situation we defined on the authentication side is as follows:\n  - any registered user will add a signed JWT to access more API endpoints.\n    \n",
    "title": "UserGateway",
    "version": "1.0.0"
  },
  "basePath": "/",
  "paths": {
    "/login": {
      "post": {
        "tags": [
          "auth"
        ],
        "operationId": "LoginUser",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/LoginRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful response",
            "schema": {
              "$ref": "#/definitions/LoginResponse"
            }
          },
          "401": {
            "$ref": "#/responses/unauthorized"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/movies": {
      "get": {
        "tags": [
          "movies"
        ],
        "operationId": "GetMovies",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "limit",
            "in": "query",
            "required": true
          },
          {
            "type": "integer",
            "format": "int32",
            "name": "offset",
            "in": "query"
          },
          {
            "enum": [
              1,
              2,
              3,
              4,
              5
            ],
            "type": "integer",
            "format": "int32",
            "description": "movie age rating according to MPAA film rating (1 - G; 2 - PG; 3 - PG_13; 4 - R; 5 - NC_17)",
            "name": "age_rating",
            "in": "query"
          },
          {
            "enum": [
              1,
              2,
              3
            ],
            "type": "integer",
            "format": "int32",
            "description": "movie catagory (1 - action; 2 - drama; 3 - thriller)",
            "name": "category",
            "in": "query"
          },
          {
            "type": "integer",
            "format": "int32",
            "description": "search query for movie release year starting point",
            "name": "from_release_year",
            "in": "query"
          },
          {
            "type": "integer",
            "format": "int32",
            "description": "search query for movie release year end point",
            "name": "to_release_year",
            "in": "query"
          },
          {
            "type": "boolean",
            "name": "only_rented",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/multipleMovies"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/movies/{movie_id}": {
      "get": {
        "tags": [
          "movies"
        ],
        "operationId": "GetMovie",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "movie_id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/singleMovie"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/movies/{movie_id}/rent": {
      "post": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "movies"
        ],
        "operationId": "RentMovie",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "movie_id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/rentMovie"
          },
          "400": {
            "$ref": "#/responses/insufficient"
          },
          "401": {
            "$ref": "#/responses/unauthorized"
          },
          "403": {
            "$ref": "#/responses/forbidden"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/profile": {
      "get": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "profile"
        ],
        "operationId": "GetProfile",
        "responses": {
          "200": {
            "description": "successful response",
            "schema": {
              "$ref": "#/definitions/Profile"
            }
          },
          "401": {
            "$ref": "#/responses/unauthorized"
          },
          "403": {
            "$ref": "#/responses/forbidden"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/profile/orders": {
      "get": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "profile"
        ],
        "operationId": "GetOrders",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "limit",
            "in": "query",
            "required": true
          },
          {
            "type": "integer",
            "format": "int32",
            "name": "offset",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/multipleOrders"
          },
          "401": {
            "$ref": "#/responses/unauthorized"
          },
          "403": {
            "$ref": "#/responses/forbidden"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/profile/payments": {
      "get": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "profile"
        ],
        "operationId": "GetPayments",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "limit",
            "in": "query",
            "required": true
          },
          {
            "type": "integer",
            "format": "int32",
            "name": "offset",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/multiplePayments"
          },
          "401": {
            "$ref": "#/responses/unauthorized"
          },
          "403": {
            "$ref": "#/responses/forbidden"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    },
    "/register": {
      "post": {
        "tags": [
          "auth"
        ],
        "operationId": "RegisterUser",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/RegisterRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful response",
            "schema": {
              "$ref": "#/definitions/RegisterResponse"
            }
          },
          "400": {
            "description": "user exists"
          },
          "default": {
            "$ref": "#/responses/otherError"
          }
        }
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "LoginRequest": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "format": "email",
          "maxLength": 256,
          "minLength": 3
        },
        "password": {
          "type": "string",
          "maxLength": 100,
          "minLength": 3
        }
      },
      "example": {
        "email": "bob@mail.ru",
        "password": "123"
      }
    },
    "LoginResponse": {
      "type": "object",
      "properties": {
        "jwt": {
          "type": "string"
        }
      }
    },
    "Movie": {
      "type": "object",
      "properties": {
        "age_rating": {
          "description": "movie age rating according to MPAA film rating (1 - G; 2 - PG; 3 - PG_13; 4 - R; 5 - NC_17)",
          "type": "integer",
          "format": "int32"
        },
        "category": {
          "description": "movie category (1 - action; 2 - drama; 3 - thriller)",
          "type": "integer",
          "format": "int32"
        },
        "content": {
          "type": "string"
        },
        "created_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "director": {
          "type": "string",
          "maxLength": 100
        },
        "duration": {
          "description": "minutes",
          "type": "integer",
          "format": "int32"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "is_paid": {
          "type": "boolean",
          "default": false
        },
        "movie_url": {
          "type": "string"
        },
        "other_data": {
          "type": "string"
        },
        "poster": {
          "type": "string"
        },
        "release_date": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "title": {
          "type": "string",
          "maxLength": 100
        },
        "updated_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Movies": {
      "type": "object",
      "properties": {
        "payload": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Movie"
          }
        },
        "total": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Order": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "float64"
        },
        "created_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "movie_id": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Orders": {
      "type": "object",
      "properties": {
        "payload": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Order"
          }
        },
        "total": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Payment": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "float64"
        },
        "created_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "status": {
          "type": "integer",
          "format": "int32"
        },
        "transaction_id": {
          "type": "integer",
          "format": "int32"
        },
        "updated_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Payments": {
      "type": "object",
      "properties": {
        "payload": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Payment"
          }
        },
        "total": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Profile": {
      "type": "object",
      "properties": {
        "balance": {
          "type": "number",
          "format": "float32"
        },
        "birth_date": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "display_name": {
          "type": "string",
          "maxLength": 50
        },
        "email": {
          "type": "string",
          "format": "email"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "phone_number": {
          "type": "string",
          "maxLength": 15,
          "pattern": "^\\d{2,15}$"
        },
        "role": {
          "description": "user role (1 - client; 2 - admin)",
          "type": "integer",
          "format": "int32"
        },
        "status": {
          "description": "user status (1 - active; 2 - blocked)",
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "RegisterRequest": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "format": "email",
          "maxLength": 256,
          "minLength": 3
        },
        "password": {
          "type": "string",
          "maxLength": 100,
          "minLength": 3
        }
      },
      "example": {
        "email": "vasia@yandex.ru",
        "password": "1234"
      }
    },
    "RegisterResponse": {
      "type": "object",
      "properties": {
        "jwt": {
          "type": "string"
        }
      }
    },
    "RentMovieResponse": {
      "type": "object",
      "properties": {
        "end_rent_time": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "movie_id": {
          "type": "integer",
          "format": "int32"
        }
      }
    }
  },
  "responses": {
    "forbidden": {
      "description": "forbidden access for a lack of sufficient privileges"
    },
    "insufficient": {
      "description": "insufficient funds"
    },
    "multipleMovies": {
      "description": "multiple movies",
      "schema": {
        "$ref": "#/definitions/Movies"
      }
    },
    "multipleOrders": {
      "description": "multiple orders",
      "schema": {
        "$ref": "#/definitions/Orders"
      }
    },
    "multiplePayments": {
      "description": "multiple payments",
      "schema": {
        "$ref": "#/definitions/Payments"
      }
    },
    "otherError": {
      "description": "other error response",
      "schema": {
        "$ref": "#/definitions/Error"
      }
    },
    "rentMovie": {
      "description": "rent movie response",
      "schema": {
        "$ref": "#/definitions/RentMovieResponse"
      }
    },
    "singleMovie": {
      "description": "single movie",
      "schema": {
        "$ref": "#/definitions/Movie"
      }
    },
    "unauthorized": {
      "description": "unauthorized access for a lack of authentication"
    }
  },
  "securityDefinitions": {
    "bearer": {
      "description": "without bearer header, just Authorization: \u003ctoken\u003e",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  }
}`))
	FlatSwaggerJSON = json.RawMessage([]byte(`{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "description": "This sample API implement basic user gateway for movie rent service.\n\nPersonas:\n  - user that want to see all movies that are free to match.\n  - registered user that want to consult personal account info and rent movies.\n\nThe situation we defined on the authentication side is as follows:\n  - any registered user will add a signed JWT to access more API endpoints.\n    \n",
    "title": "UserGateway",
    "version": "1.0.0"
  },
  "basePath": "/",
  "paths": {
    "/login": {
      "post": {
        "tags": [
          "auth"
        ],
        "operationId": "LoginUser",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/LoginRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful response",
            "schema": {
              "$ref": "#/definitions/LoginResponse"
            }
          },
          "401": {
            "description": "unauthorized access for a lack of authentication"
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/movies": {
      "get": {
        "tags": [
          "movies"
        ],
        "operationId": "GetMovies",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "limit",
            "in": "query",
            "required": true
          },
          {
            "type": "integer",
            "format": "int32",
            "name": "offset",
            "in": "query"
          },
          {
            "enum": [
              1,
              2,
              3,
              4,
              5
            ],
            "type": "integer",
            "format": "int32",
            "description": "movie age rating according to MPAA film rating (1 - G; 2 - PG; 3 - PG_13; 4 - R; 5 - NC_17)",
            "name": "age_rating",
            "in": "query"
          },
          {
            "enum": [
              1,
              2,
              3
            ],
            "type": "integer",
            "format": "int32",
            "description": "movie catagory (1 - action; 2 - drama; 3 - thriller)",
            "name": "category",
            "in": "query"
          },
          {
            "type": "integer",
            "format": "int32",
            "description": "search query for movie release year starting point",
            "name": "from_release_year",
            "in": "query"
          },
          {
            "type": "integer",
            "format": "int32",
            "description": "search query for movie release year end point",
            "name": "to_release_year",
            "in": "query"
          },
          {
            "type": "boolean",
            "name": "only_rented",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "description": "multiple movies",
            "schema": {
              "$ref": "#/definitions/Movies"
            }
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/movies/{movie_id}": {
      "get": {
        "tags": [
          "movies"
        ],
        "operationId": "GetMovie",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "movie_id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "single movie",
            "schema": {
              "$ref": "#/definitions/Movie"
            }
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/movies/{movie_id}/rent": {
      "post": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "movies"
        ],
        "operationId": "RentMovie",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "movie_id",
            "in": "path",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "rent movie response",
            "schema": {
              "$ref": "#/definitions/RentMovieResponse"
            }
          },
          "400": {
            "description": "insufficient funds"
          },
          "401": {
            "description": "unauthorized access for a lack of authentication"
          },
          "403": {
            "description": "forbidden access for a lack of sufficient privileges"
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/profile": {
      "get": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "profile"
        ],
        "operationId": "GetProfile",
        "responses": {
          "200": {
            "description": "successful response",
            "schema": {
              "$ref": "#/definitions/Profile"
            }
          },
          "401": {
            "description": "unauthorized access for a lack of authentication"
          },
          "403": {
            "description": "forbidden access for a lack of sufficient privileges"
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/profile/orders": {
      "get": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "profile"
        ],
        "operationId": "GetOrders",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "limit",
            "in": "query",
            "required": true
          },
          {
            "type": "integer",
            "format": "int32",
            "name": "offset",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "description": "multiple orders",
            "schema": {
              "$ref": "#/definitions/Orders"
            }
          },
          "401": {
            "description": "unauthorized access for a lack of authentication"
          },
          "403": {
            "description": "forbidden access for a lack of sufficient privileges"
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/profile/payments": {
      "get": {
        "security": [
          {
            "bearer": []
          }
        ],
        "tags": [
          "profile"
        ],
        "operationId": "GetPayments",
        "parameters": [
          {
            "type": "integer",
            "format": "int32",
            "name": "limit",
            "in": "query",
            "required": true
          },
          {
            "type": "integer",
            "format": "int32",
            "name": "offset",
            "in": "query"
          }
        ],
        "responses": {
          "200": {
            "description": "multiple payments",
            "schema": {
              "$ref": "#/definitions/Payments"
            }
          },
          "401": {
            "description": "unauthorized access for a lack of authentication"
          },
          "403": {
            "description": "forbidden access for a lack of sufficient privileges"
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/register": {
      "post": {
        "tags": [
          "auth"
        ],
        "operationId": "RegisterUser",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/RegisterRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful response",
            "schema": {
              "$ref": "#/definitions/RegisterResponse"
            }
          },
          "400": {
            "description": "user exists"
          },
          "default": {
            "description": "other error response",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "LoginRequest": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "format": "email",
          "maxLength": 256,
          "minLength": 3
        },
        "password": {
          "type": "string",
          "maxLength": 100,
          "minLength": 3
        }
      },
      "example": {
        "email": "bob@mail.ru",
        "password": "123"
      }
    },
    "LoginResponse": {
      "type": "object",
      "properties": {
        "jwt": {
          "type": "string"
        }
      }
    },
    "Movie": {
      "type": "object",
      "properties": {
        "age_rating": {
          "description": "movie age rating according to MPAA film rating (1 - G; 2 - PG; 3 - PG_13; 4 - R; 5 - NC_17)",
          "type": "integer",
          "format": "int32"
        },
        "category": {
          "description": "movie category (1 - action; 2 - drama; 3 - thriller)",
          "type": "integer",
          "format": "int32"
        },
        "content": {
          "type": "string"
        },
        "created_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "director": {
          "type": "string",
          "maxLength": 100
        },
        "duration": {
          "description": "minutes",
          "type": "integer",
          "format": "int32"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "is_paid": {
          "type": "boolean",
          "default": false
        },
        "movie_url": {
          "type": "string"
        },
        "other_data": {
          "type": "string"
        },
        "poster": {
          "type": "string"
        },
        "release_date": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "title": {
          "type": "string",
          "maxLength": 100
        },
        "updated_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Movies": {
      "type": "object",
      "properties": {
        "payload": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Movie"
          }
        },
        "total": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Order": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "float64"
        },
        "created_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "movie_id": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Orders": {
      "type": "object",
      "properties": {
        "payload": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Order"
          }
        },
        "total": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Payment": {
      "type": "object",
      "properties": {
        "amount": {
          "type": "number",
          "format": "float64"
        },
        "created_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "status": {
          "type": "integer",
          "format": "int32"
        },
        "transaction_id": {
          "type": "integer",
          "format": "int32"
        },
        "updated_at": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Payments": {
      "type": "object",
      "properties": {
        "payload": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Payment"
          }
        },
        "total": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "Profile": {
      "type": "object",
      "properties": {
        "balance": {
          "type": "number",
          "format": "float32"
        },
        "birth_date": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "display_name": {
          "type": "string",
          "maxLength": 50
        },
        "email": {
          "type": "string",
          "format": "email"
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "phone_number": {
          "type": "string",
          "maxLength": 15,
          "pattern": "^\\d{2,15}$"
        },
        "role": {
          "description": "user role (1 - client; 2 - admin)",
          "type": "integer",
          "format": "int32"
        },
        "status": {
          "description": "user status (1 - active; 2 - blocked)",
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "RegisterRequest": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "format": "email",
          "maxLength": 256,
          "minLength": 3
        },
        "password": {
          "type": "string",
          "maxLength": 100,
          "minLength": 3
        }
      },
      "example": {
        "email": "vasia@yandex.ru",
        "password": "1234"
      }
    },
    "RegisterResponse": {
      "type": "object",
      "properties": {
        "jwt": {
          "type": "string"
        }
      }
    },
    "RentMovieResponse": {
      "type": "object",
      "properties": {
        "end_rent_time": {
          "description": "represents seconds of UTC time since unix epoch",
          "type": "integer",
          "format": "int64"
        },
        "movie_id": {
          "type": "integer",
          "format": "int32"
        }
      }
    }
  },
  "responses": {
    "forbidden": {
      "description": "forbidden access for a lack of sufficient privileges"
    },
    "insufficient": {
      "description": "insufficient funds"
    },
    "multipleMovies": {
      "description": "multiple movies",
      "schema": {
        "$ref": "#/definitions/Movies"
      }
    },
    "multipleOrders": {
      "description": "multiple orders",
      "schema": {
        "$ref": "#/definitions/Orders"
      }
    },
    "multiplePayments": {
      "description": "multiple payments",
      "schema": {
        "$ref": "#/definitions/Payments"
      }
    },
    "otherError": {
      "description": "other error response",
      "schema": {
        "$ref": "#/definitions/Error"
      }
    },
    "rentMovie": {
      "description": "rent movie response",
      "schema": {
        "$ref": "#/definitions/RentMovieResponse"
      }
    },
    "singleMovie": {
      "description": "single movie",
      "schema": {
        "$ref": "#/definitions/Movie"
      }
    },
    "unauthorized": {
      "description": "unauthorized access for a lack of authentication"
    }
  },
  "securityDefinitions": {
    "bearer": {
      "description": "without bearer header, just Authorization: \u003ctoken\u003e",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  }
}`))
}
