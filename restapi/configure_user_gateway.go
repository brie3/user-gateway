// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"context"
	"crypto/tls"
	"net/http"

	"user-gateway/service"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/grpc/metadata"

	"user-gateway/restapi/operations"
	"user-gateway/restapi/operations/auth"
	"user-gateway/restapi/operations/movies"
	"user-gateway/restapi/operations/profile"
)

//go:generate swagger generate server --target ../../user-gateway --name UserGateway --spec ../swagger.yaml --principal interface{}

func configureFlags(api *operations.UserGatewayAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.UserGatewayAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.Service = service.New()

	// Applies when the "Authorization" header is set
	api.BearerAuth = func(token string) (interface{}, error) {
		return service.Authorize(token)
	}

	// Set your custom authorizer if needed. Default one is security.Authorized()
	// Expected interface runtime.Authorizer
	//
	// Example:
	// api.APIAuthorizer = security.Authorized()
	api.MoviesGetMovieHandler = movies.GetMovieHandlerFunc(api.Service.GetMovie)

	api.MoviesGetMoviesHandler = movies.GetMoviesHandlerFunc(api.Service.GetMovies)

	api.ProfileGetOrdersHandler = profile.GetOrdersHandlerFunc(api.Service.GetOrders)

	api.ProfileGetPaymentsHandler = profile.GetPaymentsHandlerFunc(api.Service.GetPayments)

	api.ProfileGetProfileHandler = profile.GetProfileHandlerFunc(api.Service.GetProfile)

	api.AuthLoginUserHandler = auth.LoginUserHandlerFunc(api.Service.Login)

	api.AuthRegisterUserHandler = auth.RegisterUserHandlerFunc(api.Service.Register)

	api.MoviesRentMovieHandler = movies.RentMovieHandlerFunc(api.Service.RentMovie)

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() { api.Service.Close() }

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return requestIDMiddleware(handler)
}

const key = "x-request-id"

func requestIDMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := uuid.NewV4().String()
		ctxRPC := metadata.AppendToOutgoingContext(r.Context(), key, id)
		handler.ServeHTTP(w, r.WithContext(context.WithValue(ctxRPC, service.RequestIDContextKey, id)))
	})
}
